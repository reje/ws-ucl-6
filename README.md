# re.je/ws-ucl-5

[workshop.re.je](https://workshop.re.je)

2 day workshop in Laser Scanning and Robotics


## Setup Files

The following applications are used in this workshop:

-   [Blender](https://www.blender.org/download/) - Open source 3D content-creation program
-   [Python](https://www.python.org/) - A computer programming language
-   [Recap Pro](http://www.autodesk.com/education/free-software/recap-pro) - Reality capture software and services
-   [ROS](http://www.ros.org/) - Robot Operating System (does not need to be installed)


## Main class content

-   [CleanRobot.blend](https://s3-eu-west-1.amazonaws.com/data.re.je/CleanRobot.blend) - Blender starter file
-   [3D scan video tutorial](https://youtu.be/ZVdv3f747G8) - 3D scanning workflow used in the workshop


## Example projects

-   [Box by Bot & Dolly](https://www.youtube.com/watch?v=lX6JcybgDFo)
-   [Hanson Robotics](http://www.hansonrobotics.com/)
-   [Code of Conflict](https://youtu.be/DlZhRI36nEc)


## Hardware

-   [UR10 robot](https://www.universal-robots.com/products/ur10-robot/)


## Resources

-   [Blender Setup](https://gitlab.com/reje/ws-ucl-1/blob/master/blender-config.org) - Blender Setup for Environment Projects
-   [Blender Guru](http://www.blenderguru.com/articles/free-blender-keyboard-shortcut-pdf/) [Cheatsheet](https://s3-eu-west-1.amazonaws.com/data.re.je/BlenderGuru_KeyboardShortcutGuide_v2.pdf) - Blender short-cut keys
